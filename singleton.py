"""
Singleton pattern

A class should have only one instance while providing global access to this instance
"""

class _Turbolift:

    def __str__(self):
        return "Up we go to cloud city!"
    
    def elevate(self):
        return 'Jedi temple lets go'

_instance = None #instance can be 'eagerly instantiated here to enable safe multiprocessing usage'

def Turbolift():
    global _instance
    if _instance is None:
        _instance = _Turbolift()
    return _instance