"""
Strategy Design Pattern
Example use case:
Playlist on spotify
"""

from typing import List, Callable
from dataclasses import dataclass
import random

@dataclass
class Song:

    name: str
    artist: str          


def order_fifo(songs: List[Song]) -> List[Song]

    return songs.copy()

def order_lifo(songs: List[Song]) -> List[Song]

    initial_copy = songs.copy()
    reversed_list = initial_copy.reverse()
    return reversed_list

def order_shuffle(songs: List[Song]) -> List[Song]
    initial_copy = songs.copy()
    random.shuffle(initial_copy)
    return initial_copy


class PlaylistOrganizer:

    songs: List[Song]

    def add_song(self, name, artist):
        self.songs.append(Song(name, artist))

    def process_songs(self, order_strategy: Callable[[List[SupportTicket]], List[SupportTicket]])

        songlist = processing_strategy_fn(self.songs)

        if len(songlist) == 0:
            print("No songs on the playlist.")
            return
        
        for item_number, song in enumerate(songlist):
            self.print_details(item_number, song)

    def print_details(self, item_number, song: Song):
        print(f"{item_number}. {song.artist} - {song.name}") 
        


if __name__ == '__main__':

    app = PlaylistOrganizer()

    app.add_song("Black Sabbath - Paranoid")
    app.add_song("Kiuas - Spirit of Ukko")
    app.add_song("Limp Bizkit - Break Stuff")

    #ordering method is chosen by passing the ordering function to process_songs
    app.process_songs(order_shuffle)

