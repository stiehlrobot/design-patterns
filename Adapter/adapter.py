"""Adapter design pattern example
Adapter class makes it possible for client code to call func_1 of the adapter which which then calls func_2 of Snoopy. 
"""

class Jabba:
    
    def func_1(self):
        print("Jabba here!")

class Snoopy:
    
    def func_2(self):
        print("Snoopy here!")
        
class Doopy:
    
    def func_3(self):
        print("Doopy here!")

class Adapter(Jabba, Snoopy):
    
    def func_1(self):
        "Invoking func_2 if available"
        self.func_2()
    
def client_code(target):
    target.func_1()
    

client_code(Jabba())
client_code(Adapter())
